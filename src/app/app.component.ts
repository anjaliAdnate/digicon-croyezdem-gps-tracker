import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, MenuController, ModalController, AlertController, App, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { NetworkProvider } from '../providers/network/network';
import { MenuProvider } from '../providers/menu/menu';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { SideMenuOption } from '../../shared/side-menu-content/models/side-menu-option';
import { SideMenuContentComponent } from '../../shared/side-menu-content/side-menu-content.component';
import { SideMenuSettings } from '../../shared/side-menu-content/models/side-menu-settings';
import { ReplaySubject } from "rxjs/ReplaySubject";
import { ApiServiceProvider } from '../providers/api-service/api-service';

@Component({
  selector: 'app-main-page',
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  // Get the instance to call the public methods
  @ViewChild(SideMenuContentComponent) sideMenu: SideMenuContentComponent;

  rootPage: any;
  pages: any;
  selectedMenu: any;

  islogin: any = {};
  setsmsforotp: string;
  DealerDetails: any;
  dealerStatus: any;
  dealerChk: any;

  // Options to show in the SideMenuContentComponent
  public options: Array<SideMenuOption>;

  // Settings for the SideMenuContentComponent
  public sideMenuSettings: SideMenuSettings = {
    accordionMode: true,
    showSelectedOption: true,
    selectedOptionClass: 'active-side-menu-option'
  };
  private unreadCountObservable: any = new ReplaySubject<number>(0);
  token: any;
  isDealer: any;
  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public events: Events,
    public networkProvider: NetworkProvider,
    public menuProvider: MenuProvider,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    private push: Push,
    public alertCtrl: AlertController,
    public app: App,
    public apiCall: ApiServiceProvider,
    public toastCtrl: ToastController
  ) {

    this.events.subscribe('user:updated', (udata) => {
      this.islogin = udata;
      this.isDealer = udata.isDealer;
      // console.log("islogin=> " + JSON.stringify(this.islogin));
    });


    platform.ready().then(() => {

      statusBar.styleDefault();
      statusBar.hide();
      this.splashScreen.hide();
      // let splash = modalCtrl.create(SplashPage);
      // splash.present();
    });

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("islogin=> " + JSON.stringify(this.islogin));

    this.setsmsforotp = localStorage.getItem('setsms');

    this.DealerDetails = JSON.stringify(localStorage.getItem('dealer')) || {};
    console.log("DealerDetails=> " + this.DealerDetails._id);

    this.dealerStatus = this.islogin.isDealer;
    console.log("dealerStatus=> " + this.dealerStatus);

    this.getSideMenuData();
    this.initializeApp();

    if (localStorage.getItem("loginflag")) {
      this.rootPage = DashboardPage;
    } else {
      this.rootPage = LoginPage;
    }

  }
  
  getSideMenuData() {
    this.pages = this.menuProvider.getSideMenus();
  }

  pushNotify() {
    let that = this;
    that.push.hasPermission()                       // to check if we have permission
      .then((res: any) => {

        if (res.isEnabled) {
          console.log('We have permission to send push notifications');
          that.pushSetup();
        } else {
          console.log('We do not have permission to send push notifications');
        }
      });
  }

  backBtnHandler() {
    this.platform.registerBackButtonAction(() => {

      let nav = this.app.getActiveNavs()[0];
      let activeView = nav.getActive();

      if (activeView.name === "LivePage") {

        if (nav.canGoBack()) { //Can we go back?
          nav.pop();
        } else {
          const alert = this.alertCtrl.create({
            title: 'App termination',
            message: 'Do you want to close the app?',
            buttons: [{
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                console.log('Application exit prevented!');
              }
            }, {
              text: 'Close App',
              handler: () => {
                this.platform.exitApp(); // Close this application
              }
            }]
          });
          alert.present();
        }
      }
    });
  }

  pushSetup() {
    // to initialize push notifications
    let that = this;
    const options: PushOptions = {
      android: {
        senderID: '644983599736',
        // icon: './assets/imgs/yellow-bus-icon-40028.png'
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'true'
      },
      windows: {},
      browser: {
        pushServiceURL: 'http://push.api.phonegap.com/v1/push'
      }
    };

    const pushObject: PushObject = that.push.init(options);

    pushObject.on('notification').subscribe((notification: any) => {
      if (notification.additionalData.foreground) {
        // let yourAlert = that.alertCtrl.create({
        //   title: notification.title,
        //   message: notification.message
        // });
        // yourAlert.present();
        const toast = this.toastCtrl.create({
          message: notification.message,
          duration: 2000
        });
        toast.present();
      }
    });

    pushObject.on('registration')
      .subscribe((registration: any) => {
        // alert(registration.registrationId)
        console.log("device token => " + registration.registrationId);
        // console.log("reg type=> " + registration.registrationType);
        localStorage.setItem("DEVICE_TOKEN", registration.registrationId)
      });

    pushObject.on('error').subscribe(error => {
      console.error('Error with Push plugin', error)
      // alert('Error with Push plugin' + error)
    });
  }

  initializeApp() {
    let that = this;
    that.platform.ready().then(() => {

      that.pushNotify();

      that.networkProvider.initializeNetworkEvents();

      // Offline event
      that.events.subscribe('network:offline', () => {
        // alert('network:offline ==> ' + this.networkProvider.getNetworkType());
        alert("Internet is not connected... please make sure the internet connection is working properly.")
      });

      // Online event
      that.events.subscribe('network:online', () => {
        alert('network:online ==> ' + this.networkProvider.getNetworkType());
      });

      that.backBtnHandler();
    });

    // Initialize some options
    that.initializeOptions();



    // Change the value for the batch every 5 seconds
    setInterval(() => {
      this.unreadCountObservable.next(Math.floor(Math.random() * 10) + 1);
    }, 5000);
  }


  private initializeOptions(): void {
    this.options = new Array<SideMenuOption>();
    // debugger;
    // Load simple menu options analytics
    // ------------------------------------------
    if (this.isDealer == true) {
      this.options.push({
        iconName: 'home',
        displayText: 'Home',
        component: 'DashboardPage',
      });

      this.options.push({
        iconName: 'people',
        displayText: 'Groups',
        component: 'GroupsPage'
      });

      this.options.push({
        iconName: 'contacts',
        displayText: 'Customers',
        component: 'CustomersPage'
      });

      this.options.push({
        iconName: 'notifications',
        displayText: 'Notifications',
        component: 'AllNotificationsPage'
      });

      // Load options with nested items (with icons)
      // -----------------------------------------------
      this.options.push({
        displayText: 'Reports',
        // iconName: 'clipboard',
        suboptions: [
          {
            iconName: 'clipboard',
            displayText: 'Daily Report',
            component: 'DailyReportPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Summary Report',
            component: 'DeviceSummaryRepoPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Geofenceing Report',
            component: 'GeofenceReportPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Overspeed Report',
            component: 'OverSpeedRepoPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Ignition Report',
            component: 'IgnitionReportPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Stoppage Report',
            component: 'StoppagesRepoPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Fuel Report',
            component: 'FuelReportPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Distance Report',
            component: 'DistanceReportPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Trip Report',
            component: 'TripReportPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Route Violation Report',
            component: 'RouteVoilationsPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Speed Variation Report',
            component: 'SpeedRepoPage'
          }
        ]
      });

      // Load options with nested items (without icons)
      // -----------------------------------------------
      this.options.push({
        displayText: 'Routes',
        iconName: 'map',
        component: 'RoutePage'
      });

      // Load special options
      // -----------------------------------------------
      this.options.push({
        displayText: 'Customer Support',
        suboptions: [
          {
            iconName: 'star',
            displayText: 'Rate Us',
            component: 'FeedbackPage'
          },
          {
            iconName: 'mail',
            displayText: 'Contact Us',
            component: 'ContactUsPage'
          },
          // {
          //   iconName: 'alert',
          //   displayText: 'About Us',
          //   component: 'AboutUsPage'
          // }
        ]
      });
      this.options.push({
        displayText: 'Profile',
        iconName: 'person',
        component: 'ProfilePage'
      });
    } else {
      // if (this.isDealer == false) {
      this.options.push({
        iconName: 'home',
        displayText: 'Home',
        component: 'DashboardPage',
      });

      this.options.push({
        iconName: 'people',
        displayText: 'Groups',
        component: 'GroupsPage'
      });

      this.options.push({
        iconName: 'people',
        displayText: 'Dealers',
        component: 'DashboardPage'
      });

      this.options.push({
        iconName: 'notifications',
        displayText: 'Notifications',
        component: 'AllNotificationsPage'
      });

      // Load options with nested items (with icons)
      // -----------------------------------------------
      this.options.push({
        displayText: 'Reports',
        // iconName: 'clipboard',
        suboptions: [
          {
            iconName: 'clipboard',
            displayText: 'Daily Report',
            component: 'DailyReportPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Summary Report',
            component: 'DeviceSummaryRepoPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Geofenceing Report',
            component: 'GeofenceReportPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Overspeed Report',
            component: 'OverSpeedRepoPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Ignition Report',
            component: 'IgnitionReportPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Stoppage Report',
            component: 'StoppagesRepoPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Fuel Report',
            component: 'FuelReportPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Distnace Report',
            component: 'DistanceReportPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Trip Report',
            component: 'TripReportPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Route Violation Report',
            component: 'RouteVoilationsPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Speed Variation Report',
            component: 'SpeedRepoPage'
          }
        ]
      });

      // Load options with nested items (without icons)
      // -----------------------------------------------
      this.options.push({
        displayText: 'Routes',
        iconName: 'map',
        component: 'RoutePage'
      });

      // Load special options
      // -----------------------------------------------
      this.options.push({
        displayText: 'Customer Support',
        suboptions: [
          {
            iconName: 'star',
            displayText: 'Rate Us',
            component: 'FeedbackPage'
          },
          {
            iconName: 'mail',
            displayText: 'Contact Us',
            component: 'ContactUsPage'
          },
          // {
          //   iconName: 'alert',
          //   displayText: 'About Us',
          //   component: 'AboutUsPage'
          // }
        ]
      });

      this.options.push({
        displayText: 'Profile',
        iconName: 'person',
        component: 'ProfilePage'
      });
    }
    // }


    // this.events.subscribe('user:updated', data => {
    //   // if(data.isDealer)
    //   debugger;
    //   console.log("check if dealer in component=> ", data.isDealer)
    //   if (data.isDealer === false) {
    //     this.options[2].displayText = 'Dealers';
    //     this.options[2].iconName = 'person';
    //     this.options[2].component = 'DashboardPage';
    //   } else {
    //     this.events.subscribe("sidemenu:event", data => {
    //       console.log("sidemenu:event=> ", data);
    //       if (data) {
    //         this.options[2].displayText = 'Dealers';
    //         this.options[2].iconName = 'person';
    //         this.options[2].component = 'DashboardPage';
    //       }
    //     });
    //   }

    // })
    // debugger;

    // this.events.subscribe('user:updated', (udata) => {
    //   this.islogin = udata;
    //   console.log("islogin=> " + JSON.stringify(this.islogin));
    //   console.log("isDealer=> ", udata.isDealer)
    //   if (udata.isDealer === false) {
    //     this.options[2].displayText = 'Dealers';
    //     this.options[2].iconName = 'person';
    //     this.options[2].component = 'DashboardPage';
    //   }
    // });

    console.log("check localstorage for is dealer value=> ", localStorage.getItem("isDealervalue"))
    var checkCon = localStorage.getItem("isDealervalue");

    if (checkCon != null) {
      if (checkCon == 'true') {
        console.log("console=> ", localStorage.getItem("isDealervalue"))
        this.options[2].displayText = 'Dealers';
        this.options[2].iconName = 'person';
        this.options[2].component = 'DashboardPage';
      } else if (checkCon == 'false') {
        this.options[2].displayText = 'Customers';
        this.options[2].iconName = 'contacts';
        this.options[2].component = 'CustomersPage';
      }
    }

    this.events.subscribe("sidemenu:event", data => {
      console.log("sidemenu:event=> ", data);
      if (data) {
        this.options[2].displayText = 'Dealers';
        this.options[2].iconName = 'person';
        this.options[2].component = 'DashboardPage';
      }
    });

  }

  public onOptionSelected(option: SideMenuOption): void {
    this.menuCtrl.close().then(() => {
      if (option.custom && option.custom.isLogin) {
        this.presentAlert('You\'ve clicked the login option!');
      } else if (option.custom && option.custom.isLogout) {
        this.presentAlert('You\'ve clicked the logout option!');
      } else if (option.custom && option.custom.isExternalLink) {
        let url = option.custom.externalUrl;
        window.open(url, '_blank');
      } else {
        // Get the params if any
        const params = option.custom && option.custom.param;
        console.log("side menu click event=> " + option.component)
        // Redirect to the selected page
        if (localStorage.getItem("isDealervalue") && option.component == 'DashboardPage') {

          localStorage.setItem('details', localStorage.getItem('dealer'));
          localStorage.setItem('isDealervalue', 'false');
        }
        this.nav.setRoot(option.component, params);
      }
    });
  }

  public collapseMenuOptions(): void {
    this.sideMenu.collapseAllOptions();
  }

  public presentAlert(message: string): void {
    let alert = this.alertCtrl.create({
      title: 'Information',
      message: message,
      buttons: ['Ok']
    });
    alert.present();
  }

  openPage(page, index) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if (page.component) {
      this.nav.setRoot(page.component);
      this.menuCtrl.close();
    } else {
      if (this.selectedMenu) {
        this.selectedMenu = 0;
      } else {
        this.selectedMenu = index;
      }
    }
  }

  logout() {

    this.token = localStorage.getItem("DEVICE_TOKEN");

    var pushdata = {
      "uid": this.islogin._id,
      "token": this.token,
      "os": "android"
    }

    ///////////////////////////////
    let alert = this.alertCtrl.create({
      message: 'Do you want to logout from the application?',
      buttons: [{
        text: 'Yes',
        handler: () => {
          this.apiCall.startLoading().present();
          this.apiCall.pullnotifyCall(pushdata)
            .subscribe(data => {
              this.apiCall.stopLoading();
              console.log("push notifications updated " + data.message)
              localStorage.clear();
              localStorage.setItem('count', null)
              this.menuCtrl.close();
              this.nav.setRoot(LoginPage);

            },
              err => {
                this.apiCall.stopLoading();
                console.log(err)
              });
        }
      },
      {
        text: 'No',
        handler: () => {
          this.menuCtrl.close();
        }
      }]
    });
    alert.present();

    ///////////////////////////////

  }

  chkCondition() {
    // debugger;

    this.events.subscribe("event_sidemenu", data => {
      var sidemenuOption = JSON.parse(data);
      // console.log(data);
      console.log(sidemenuOption);

      this.islogin = JSON.parse(data);
      console.log("islogin event publish=> " + this.islogin);

      this.options[2].displayText = 'Dealers';
      this.options[2].iconName = 'person';
      this.options[2].component = 'DashboardPage';

      // this.events.publish("sidemenu:event", this.options)
      this.initializeOptions();
      console.log("options=> " + JSON.stringify(this.options[2]))
    })

    // this.events.subscribe('user:updated', (udata) => {
    //   this.islogin = udata;
    //   console.log("islogin=> " + JSON.stringify(this.islogin));
    //   console.log("isDealer=> ", udata.isDealer)
    //   if (udata.isDealer === false) {
    //     this.options[2].displayText = 'Dealers';
    //     this.options[2].iconName = 'person';
    //     this.options[2].component = 'DashboardPage';
    //   }
    //   this.initializeOptions();
    //   console.log(JSON.stringify(this.options))
    // });



    this.initializeOptions();

    this.dealerChk = localStorage.getItem('condition_chk');
  }

  // test() {
  //   if (this.dealerStatus) {
  //     this.options[2].displayText = 'Costumer';
  //     this.options[2].iconName = 'person';
  //     this.options[2].component = 'CustomersPage';
  //   }

  // }

}
