import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
declare var google;

@IonicPage()
@Component({
  selector: 'page-distance-report',
  templateUrl: 'distance-report.html',
})
export class DistanceReportPage implements OnInit{

  locationAddress: any;
  locationEndAddress: any;
  distanceReport: any;
  Ignitiondevice_id: any;
  datetimeEnd: string;
  datetimeStart: string;
  islogin: any;
  devices1243: any[];
  devices: any;
  portstemp: any;
  datetime: number;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public apicallDistance: ApiServiceProvider) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);
    // var temp = new Date();
    // this.datetimeStart = temp.toISOString();
    // // $rootScope.datetimeStart.setHours(0,0,0,0);
    // this.datetimeEnd = temp.toISOString();
    var temp = new Date();
    
    // this.datetimeStart = temp.toISOString();
    var settime= temp.getTime();
    // this.datetime=new Date(settime).setMinutes(0);
    this.datetime=new Date(settime).setHours(5,30,0);

     this.datetimeStart =new Date(this.datetime).toISOString();
     
     var a= new Date()
    a.setHours(a.getHours() + 5);
    a.setMinutes(a.getMinutes()+30);
    this.datetimeEnd = new Date(a).toISOString();
  }


  ngOnInit() {
    this.getdevices();
  }
  
  change(datetimeStart) {
    console.log(datetimeStart);
  }

  change1(datetimeEnd) {
    console.log(datetimeEnd);
  }
  getdevices() {
    this.apicallDistance.startLoading().present();
    this.apicallDistance.livedatacall(this.islogin._id, this.islogin.email)
      .subscribe(data => {
        this.apicallDistance.stopLoading();
        this.devices1243 = [];
        this.devices = data;
        this.portstemp = data.devices;
        this.devices1243.push(data);
        console.log(this.devices1243);
        localStorage.setItem('devices1243', JSON.stringify(this.devices1243));
        this.devices = localStorage.getItem('devices1243');
        for (var i = 0; i < this.devices1243[i]; i++) {
          this.devices1243[i] = {
            'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
          };
        }
      },
        err => {
          this.apicallDistance.stopLoading();
          console.log(err);
        });
  }


  getdistancedevice(from, to, selectedVehicle) {
    console.log("selectedVehicle=> ", selectedVehicle)
    this.Ignitiondevice_id = selectedVehicle._id;
    // this.getIgnitiondeviceReport(from, to);
  }


  
  distanceReportData = [];
  getDistanceReport(starttime, endtime) {
    var outerthis = this;
    if (this.Ignitiondevice_id == undefined) {
      this.Ignitiondevice_id = "";

    }
    console.log(starttime);
    console.log(endtime);

    this.apicallDistance.startLoading().present();
    this.apicallDistance.getDistanceReportApi(starttime, endtime, this.islogin._id, this.Ignitiondevice_id)
      .subscribe(data => {
        this.apicallDistance.stopLoading();
        this.distanceReport = data;
        console.log(this.distanceReport);

        var i = 0, howManyTimes = this.distanceReport.length;
        function f() {

          outerthis.distanceReportData.push({ 'distance': outerthis.distanceReport[i].distance, 'Device_Name': outerthis.distanceReport[i].device.Device_Name });
          if (outerthis.distanceReport[i].endPoint != null && outerthis.distanceReport[i].startPoint != null) {
            var latEnd = outerthis.distanceReport[i].endPoint[0];
            var lngEnd = outerthis.distanceReport[i].endPoint[1];
            var latlng = new google.maps.LatLng(latEnd, lngEnd);

            var latStart = outerthis.distanceReport[i].startPoint[0];
            var lngStart = outerthis.distanceReport[i].startPoint[1];


            var lngStart1 = new google.maps.LatLng(latStart, lngStart);

            var geocoder = new google.maps.Geocoder();

            var request = {
              latLng: latlng
            };

            var request1 = {
              latLng: lngStart1
            };


            geocoder.geocode(request, function (data, status) {
              console.log(data);
              if (status == google.maps.GeocoderStatus.OK) {
                if (data[1] != null) {
                  console.log("End address is: " + data[1].formatted_address);
                  outerthis.locationEndAddress = data[1].formatted_address;

                } else {
                  console.log("No address available")
                }
              }
              else {
                console.log(" address available")
              }
              outerthis.distanceReportData[outerthis.distanceReportData.length - 1].EndLocation = outerthis.locationEndAddress;
            })

            geocoder.geocode(request1, function (data, status) {
              console.log(data);
              if (status == google.maps.GeocoderStatus.OK) {
                if (data[1] != null) {
                  console.log("Start address is: " + data[1].formatted_address);
                  outerthis.locationAddress = data[1].formatted_address;

                } else {
                  console.log("No address available")
                }
              }
              else {
                console.log(" address available")
              }
              outerthis.distanceReportData[outerthis.distanceReportData.length - 1].StartLocation = outerthis.locationAddress;
            })

          }
          console.log(outerthis.distanceReportData);

          i++;
          if (i < howManyTimes) {
            setTimeout(f, 100);
          }

        }
        f();

      }, error => {
        this.apicallDistance.stopLoading();
        console.log(error);
      })
  }

}
