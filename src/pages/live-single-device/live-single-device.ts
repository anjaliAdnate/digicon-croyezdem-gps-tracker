import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { NavParams, Navbar, NavController } from "ionic-angular";
import * as io from 'socket.io-client';
// import * as moment from 'moment';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as _ from "lodash";
import { GoogleMaps, Marker, LatLng, Spherical, GeocoderResult, Geocoder, GoogleMapsEvent } from '@ionic-native/google-maps';
// import { LivePage } from "../live/live";

@Component({
    selector: 'page-live-single-device',
    templateUrl: './live-single-device.html'
})
export class LiveSingleDevice implements OnInit, OnDestroy {

    drawerHidden = false;
    shouldBounce = true;
    dockedHeight = 150;
    bounceThreshold = 500;
    distanceTop = 56;
    // onClickShow: boolean;

    titleText: string;
    deviceData: any;
    userdetails: any;
    socketSwitch: any = {};
    socketChnl: any = [];
    _io: any;

    car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';
    bike = "M419 1754 l3 -44 -37 0 c-21 0 -34 4 -30 10 3 6 -1 10 -9 10 -19 0 -66 -21 -66 -30 0 -18 44 -44 71 -42 16 1 29 0 29 -2 0 -12 -24 -56 -30 -56 -4 0 -7 -6 -7 -12 0 -7 -8 -29 -16 -48 l-15 -35 -7 30 c-4 17 -8 36 -11 43 -6 19 -40 14 -69 -11 l-27 -23 16 -134 c12 -99 23 -149 41 -186 28 -57 25 -84 -10 -84 -34 0 -51 -25 -31 -45 9 -9 16 -33 16 -53 0 -30 -9 -46 -44 -84 -24 -27 -42 -48 -40 -48 2 0 -10 -21 -26 -47 -17 -27 -30 -61 -30 -79 0 -35 35 -189 52 -232 6 -15 10 -41 9 -59 -3 -34 19 -53 58 -53 12 0 21 -6 21 -14 0 -12 -7 -13 -32 -5 -44 12 -70 11 -66 -2 2 -6 21 -12 41 -14 21 -1 43 -7 49 -13 17 -17 23 -119 8 -137 -10 -12 -9 -19 3 -38 l15 -23 -46 23 c-29 15 -63 23 -94 23 -56 0 -61 -14 -17 -48 24 -20 42 -24 97 -24 63 -1 69 -3 109 -39 22 -21 38 -41 35 -44 -4 -4 -2 -5 3 -4 6 1 30 -3 55 -10 54 -16 142 -9 176 14 13 8 24 13 24 10 0 -3 16 13 36 34 35 39 36 40 103 39 59 -1 73 2 99 23 45 35 41 49 -17 49 -32 0 -64 -8 -97 -25 -27 -14 -42 -20 -33 -13 10 8 14 23 11 38 -10 54 -7 139 6 152 7 7 30 13 52 13 27 0 40 4 40 13 0 11 -9 13 -36 8 -19 -4 -42 -10 -50 -13 -10 -3 -12 0 -8 10 3 8 18 17 33 20 23 5 30 13 35 41 4 18 13 47 20 62 8 16 26 77 41 136 l26 106 -45 90 c-24 49 -56 102 -70 119 -14 17 -23 33 -19 37 3 3 1 6 -5 6 -15 0 -16 57 -1 62 22 7 -3 38 -31 38 -19 0 -31 6 -34 18 -7 20 38 114 49 104 5 -4 5 -2 2 4 -4 7 0 67 9 135 8 68 13 134 10 145 -8 29 -60 51 -81 34 -8 -7 -15 -22 -15 -35 0 -43 -13 -33 -46 36 -19 39 -34 73 -34 76 0 3 13 3 30 1 22 -3 35 2 50 17 27 29 17 49 -26 53 -22 1 -33 -2 -29 -8 4 -6 -8 -10 -29 -10 -34 0 -35 1 -38 43 -3 42 -3 42 -43 43 l-40 1 4 -43z m-140 -187 c13 -16 5 -27 -21 -27 -14 0 -18 -5 -14 -17 8 -29 26 -196 24 -220 -5 -48 -19 -2 -32 106 -16 127 -15 133 3 155 16 20 25 20 40 3z m402 -3 c7 -9 14 -22 15 -30 4 -26 -25 -237 -34 -246 -12 -12 -12 12 0 127 6 55 16 103 22 107 6 4 -2 12 -22 19 -26 9 -30 14 -21 25 15 18 24 18 40 -2z m-123 -226 c48 -23 54 -30 46 -51 -5 -13 -10 -14 -39 -1 -50 21 -163 18 -207 -5 -35 -17 -37 -17 -41 0 -7 24 23 49 85 70 58 20 89 18 156 -13z m28 -469 c39 -36 56 -76 56 -134 2 -169 -205 -230 -284 -85 -26 47 -21 155 7 176 10 8 15 14 10 14 -13 0 27 41 50 51 11 5 46 7 77 6 45 -3 64 -9 84 -28z m131 -37 c7 -8 10 -19 7 -23 -3 -5 -1 -9 4 -9 17 0 21 -41 10 -103 -10 -59 -28 -100 -28 -64 0 14 -44 137 -75 209 -6 15 -2 16 32 11 21 -3 43 -12 50 -21z m-455 -49 c-15 -38 -30 -71 -35 -74 -5 -3 -8 -9 -8 -13 3 -21 0 -36 -8 -36 -5 0 -11 30 -13 68 -5 61 -3 69 20 95 14 15 36 27 48 27 l22 0 -26 -67z m377 -317 c19 14 25 14 32 3 13 -21 12 -23 -21 -42 -16 -10 -30 -23 -30 -28 0 -6 -4 -8 -8 -5 -4 2 -9 -9 -10 -25 -2 -22 2 -29 16 -29 14 0 21 -12 29 -44 10 -43 9 -45 -13 -43 -13 1 -29 10 -37 21 -13 17 -15 14 -37 -40 -13 -33 -30 -65 -38 -72 -9 -6 -12 -12 -7 -12 4 0 0 -9 -10 -20 -35 -39 -107 -11 -117 45 -1 11 -9 26 -15 33 -12 12 -17 33 -15 54 1 3 -5 11 -13 18 -11 9 -16 7 -25 -9 -12 -23 -50 -29 -50 -7 0 44 14 76 32 76 17 0 19 4 13 23 -4 12 -7 30 -8 39 -1 9 -4 15 -8 12 -4 -2 -19 6 -33 18 -19 16 -23 27 -17 37 8 12 13 12 34 -2 l25 -16 7 39 c4 22 8 40 9 40 1 0 18 -9 39 -19 l37 -19 -28 -8 c-15 -4 -30 -7 -33 -6 -3 0 -6 -11 -5 -26 l1 -27 125 0 c120 0 125 1 128 21 2 16 -5 25 -27 34 l-31 12 33 22 32 22 10 -43 c10 -43 10 -44 34 -27z m-161 11 c2 -10 -3 -17 -12 -17 -18 0 -29 16 -21 31 9 14 29 6 33 -14z m-58 -11 c0 -16 -18 -31 -27 -22 -8 8 5 36 17 36 5 0 10 -6 10 -14z m108 -10 c3 -12 -1 -17 -10 -14 -7 3 -15 13 -16 22 -3 12 1 17 10 14 7 -3 15 -13 16 -22z m-148 -5 c0 -14 -18 -23 -31 -15 -8 4 -7 9 2 15 18 11 29 11 29 0z m198 -3 c-3 -7 -11 -13 -18 -13 -7 0 -15 6 -17 13 -3 7 4 12 17 12 13 0 20 -5 18 -12z m-430 -224 c37 -10 25 -23 -14 -14 -19 5 -38 11 -41 14 -8 8 26 7 55 0z m672 -3 c-8 -5 -35 -11 -60 -15 l-45 -6 35 14 c39 16 96 21 70 7z m-454 -113 l29 -30 -30 15 c-17 8 -38 24 -48 35 -18 20 -18 20 1 15 11 -2 33 -18 48 -35z m239 15 c-11 -8 -33 -21 -50 -30 l-30 -15 30 31 c16 17 35 29 42 27 7 -3 13 0 13 6 0 6 3 8 7 4 5 -4 -1 -14 -12 -23z"
    truck = 'M180 1725 c0 -8 -21 -25 -47 -37 -27 -12 -58 -31 -70 -42 l-23 -20 0 -495 0 -496 23 -18 c12 -10 31 -24 42 -32 20 -13 20 -14 -2 -37 -21 -22 -23 -34 -23 -131 0 -65 -4 -107 -10 -107 -5 0 -10 -7 -10 -15 0 -8 5 -15 10 -15 6 0 10 -31 10 -74 0 -44 5 -78 11 -82 8 -4 7 -9 -2 -15 -38 -23 96 -89 181 -89 85 0 219 66 181 89 -9 6 -10 11 -2 15 6 4 11 38 11 82 0 43 4 74 10 74 6 0 10 7 10 15 0 8 -4 15 -10 15 -6 0 -10 43 -10 108 0 96 -2 108 -20 120 -30 18 -24 38 20 69 l40 28 0 491 0 491 -22 24 c-22 24 -86 59 -106 59 -5 0 -12 9 -15 20 -5 18 -14 20 -91 20 -70 0 -86 -3 -86 -15z m115 -101 c21 -8 35 -45 28 -72 -7 -29 -47 -44 -78 -30 -29 13 -38 36 -29 72 7 30 43 44 79 30z m-148 -726 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m250 0 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m-102 326 c21 -9 35 -46 28 -74 -12 -44 -93 -46 -108 -1 -17 54 27 95 80 75z m20 -436 c2 -2 6 -15 10 -30 10 -38 -26 -72 -68 -64 -37 8 -35 6 -43 43 -8 42 18 67 64 59 19 -3 36 -6 37 -8z m18 -245 c-36 -16 -102 -16 -130 0 -18 10 -7 12 68 12 80 0 87 -1 62 -12z m-203 -69 c0 -21 -4 -33 -10 -29 -5 3 -10 19 -10 36 0 16 5 29 10 29 6 0 10 -16 10 -36z m300 1 c0 -24 -5 -35 -15 -35 -15 0 -22 50 -8 63 15 15 23 5 23 -28z m-300 -87 c0 -24 -5 -50 -10 -58 -7 -11 -10 2 -10 43 0 31 4 57 10 57 6 0 10 -19 10 -42z m300 -20 c0 -43 -3 -58 -10 -48 -13 20 -23 87 -15 100 15 25 25 5 25 -52z m-201 -44 c31 -4 80 -2 108 4 59 10 70 5 75 -43 3 -29 2 -30 -57 -38 -58 -8 -199 -2 -229 9 -12 5 -12 11 2 45 11 28 20 38 30 34 9 -3 40 -8 71 -11z m147 -222 c-14 -15 -15 -13 -3 17 8 23 13 28 15 16 2 -10 -3 -25 -12 -33z m-213 18 c3 -11 1 -20 -4 -20 -5 0 -9 9 -9 20 0 11 2 20 4 20 2 0 6 -9 9 -20z m267 -25 c0 -2 -10 -10 -22 -16 -21 -11 -22 -11 -9 4 13 16 31 23 31 12z'
    // car = 'https://png.icons8.com/dusk/2x/car.png';
    carIcon = {
        path: this.car,
        scale: .6,
        strokeColor: 'white',
        strokeWeight: .10,
        fillOpacity: 1,
        fillColor: 'blue',
        offset: '5%',
        anchor: [10, 25], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
        rotation: 0
    };

    bikeIcon = {
        path: this.bike,
        scale: .019,
        strokeColor: 'black',
        strokeWeight: .90,
        fillOpacity: 0.8,
        fillColor: '#870000',
        offset: '0%',
        anchor: [400, 900], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
        rotation: 0
    };

    truckIcon = {
        path: this.truck,
        scale: .03,
        strokeColor: 'black',
        strokeWeight: .90,
        fillOpacity: 0.8,
        fillColor: '#00630d',
        offset: '0%',
        anchor: [250, 700], // orig 10,50 back of car, 10,0 front of car, 10,25 center of car,
        rotation: 0
    };

    icons = {
        "car": this.carIcon,
        "bike": this.bikeIcon,
        "truck": this.truckIcon
    }
    allData: any = {};
    vehicle_speed: any;
    todays_odo: any;
    total_odo: any;
    fuel: any;
    lastStoppedAt: string;
    distFromLastStop: any;
    timeAtLastStop: any;
    today_stopped: any;
    today_running: any;
    last_ACC: any;
    currentFuel: any;
    power: any;
    gpsTracking: any;
    liveVehicleName: any;
    address: any;
    @ViewChild(Navbar) navBar: Navbar;
    constructor(
        public navParams: NavParams,
        public apiCall: ApiServiceProvider,
        public navCtrl: NavController
    ) {
        console.log("nav params=> " + JSON.stringify(this.navParams.get("param")))
        this.deviceData = this.navParams.get("param");
        this.titleText = this.deviceData.Device_Name;
    }

    ionViewDidLoad() {
        this.navBar.backButtonClick = (e: UIEvent) => {
            console.log("nav pop")
            // todo something
            this.navCtrl.pop();
            // this.navCtrl.push(LivePage);
        }
    }

    ngOnInit() {
        let that = this;
        // that.drawerHidden = true;
        // that.onClickShow = false;

        console.log("coming soon")
        that._io = io.connect('https://www.oneqlik.in/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
        that._io.on('connect', (data) => {
            console.log('IO Connected', data);
        });
        that.temp(that.deviceData);
    }

    ngOnDestroy() {
        let that = this;
        console.log("socket channel=> ", that.socketChnl[0])
        for (var i = 0; i < that.socketChnl.length; i++)
            that._io.removeAllListeners(that.socketChnl[i]);
    }

    temp(data) {
        let that = this;

        // that.drawerHidden = true;
        // that.onClickShow = false;

        // console.log("on select change data=> " + JSON.stringify(data));
        // for (var i = 0; i < that.socketChnl.length; i++)
        //     that._io.removeAllListeners(that.socketChnl[i]);

        // that.allData = {};
        // that.socketChnl = [];
        // that.socketSwitch = {};
        if(that.allData.map != undefined) {
            that.allData.map.clear();
        }
        if (data) {
            if (data.last_location) {
                that.allData.map = GoogleMaps.create('map_canvas_single_device', {
                    camera: {
                        target: {
                            lat: data.last_location['lat'],
                            lng: data.last_location['long']
                        },
                        zoom: 15,
                        tilt: 30
                    }
                });
                that.allData.map.setTrafficEnabled(true);
                that.socketInit(data);
            } else {
                that.allData.map = GoogleMaps.create('map_canvas_single_device', {
                    camera: {
                        target: {
                            lat: 18.5865673,
                            lng: 73.6954015
                        },
                        zoom: 15,
                        tilt: 15
                    }
                });
                that.allData.map.setTrafficEnabled(true);

                that.socketInit(data);
            }
        }
    }

    parseMillisecondsIntoReadableTime(milliseconds) {
        //Get hours from milliseconds
        var hours = milliseconds / (1000 * 60 * 60);
        var absoluteHours = Math.floor(hours);
        var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;

        //Get remainder from hours and convert to minutes
        var minutes = (hours - absoluteHours) * 60;
        var absoluteMinutes = Math.floor(minutes);
        var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;

        //Get remainder from minutes and convert to seconds
        var seconds = (minutes - absoluteMinutes) * 60;
        var absoluteSeconds = Math.floor(seconds);
        var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;

        // return h + ':' + m;

        return h + ':' + m + ':' + s;
    }

    socketInit(pdata, center = false) {
        // debugger;
        console.log("pdate on single=> ", pdata);
        this._io.emit('acc', pdata.Device_ID);
        this.socketChnl.push(pdata.Device_ID + 'acc');
        this._io.on(pdata.Device_ID + 'acc', (d1, d2, d3, d4, d5) => {
            // console.log("d4=> " + d4)
            let that = this;
            if (d4 != undefined)
                (function (data) {
                    if (data == undefined) {
                        return;
                    }
                    // console.log("socketSwitch=> " + JSON.stringify(that.socketSwitch))
                    if (data._id != undefined && data.last_location != undefined) {
                        var key = data._id;
                        let ic = _.cloneDeep(that.icons[data.iconType]);
                        if (!ic) {
                            console.warn(data.Device_ID + " has no iconType", data.iconType);
                            return;
                        }
                        ic.path = null;
                        // ic.url = 'https://www.oneqlik.in/images/' + data.status.toLowerCase() + data.iconType + '.png#' + data._id;
                        ic.url = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                        console.log("icon url=> ", ic.url);
                        console.log("all data => ", data);
                        that.vehicle_speed = data.last_speed;
                        that.todays_odo = data.today_odo;
                        that.total_odo = data.total_odo;
                        that.fuel = data.currentFuel;

                        // that.lastStoppedAt = data.lastStoppedAt;

                        var fd = new Date(data.lastStoppedAt).getTime();
                        var td = new Date().getTime();
                        console.log("fd=> " + fd)
                        console.log("td=> " + td)
                        var time_difference = td - fd;
                        var total_min = time_difference / 60000;
                        var hours = total_min / 60
                        var rhours = Math.floor(hours);
                        var minutes = (hours - rhours) * 60;
                        var rminutes = Math.round(minutes);
                        that.lastStoppedAt = rhours + ':' + rminutes;

                        that.distFromLastStop = data.distFromLastStop;
                        // console.log("parse time=> " + that.parseMillisecondsIntoReadableTime(data.timeAtLastStop))
                        that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(data.timeAtLastStop);
                        that.today_stopped = that.parseMillisecondsIntoReadableTime(data.today_stopped);
                        that.today_running = that.parseMillisecondsIntoReadableTime(data.today_running);

                        that.last_ACC = data.last_ACC;
                        that.currentFuel = data.currentFuel;
                        that.power = data.power;
                        that.gpsTracking = data.gpsTracking;

                        // console.log(new Date(data.timeAtLastStop).toISOString)

                        console.log(data.Device_Name, "_", data.status, "_", data.last_speed);
                        // console.log("imp data=> " + JSON.stringify(data))
                        console.log("alldata key=> ", that.allData[key]);

                        if (that.allData[key]) {
                            that.socketSwitch[key] = data;
                            // that.allData[key].mark.remove();
                            that.allData[key].mark.clear();
                            that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                            that.allData[key].mark.setIcon(ic.url)
                            var temp = _.cloneDeep(that.allData[key].poly[1]);
                            that.allData[key].poly[0] = _.cloneDeep(temp);
                            that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };
                            var speed = data.status == "RUNNING" ? data.last_speed : 0;
                            that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, data.iconType, data.status, that);
                            // that.allData.map.setAllowConcurrentMultipleOpenInfoWindows(true);

                            // that.allData.map.addMarker({
                            //     icon: ic.url,
                            //     styles: {
                            //         'text-align': 'center',
                            //         'font-style': 'italic',
                            //         'font-weight': 'bold',
                            //         'color': 'red'
                            //     },
                            //     position: that.allData[key].poly[1],
                            // }).then((marker: Marker) => {
                            //     //   if (that.selectedVehicle != undefined) {
                            //     marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
                            //         .subscribe(e => {
                            //             // alert(e.address);
                            //             console.log(e)
                            //             that.liveVehicleName = data.Device_Name;
                            //             that.drawerHidden = false;
                            //             that.onClickShow = true;
                            //         });
                            //     //   }
                            //     // that.allData.map.setVisible(true);
                            //     that.allData[key].mark = marker;
                            //     // marker.setTitle(data.Device_Name);
                            //     // marker.showInfoWindow();

                            //     var temp = _.cloneDeep(that.allData[key].poly[1]);
                            //     that.allData[key].poly[0] = _.cloneDeep(temp);
                            //     that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };
                            //     var speed = data.status == "RUNNING" ? data.last_speed : 0;
                            //     that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, data.iconType, data.status, that);
                            // });

                        } else {
                            that.allData[key] = {};
                            that.socketSwitch[key] = data;
                            that.allData[key].poly = [];
                            if (data.sec_last_location) {
                                that.allData[key].poly.push({ lat: data.sec_last_location.lat, lng: data.sec_last_location.long });
                            } else {
                                that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                            }
                            that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                            if (data.last_location != undefined) {

                                that.allData.map.addMarker({
                                    icon: ic.url,
                                    styles: {
                                        'text-align': 'center',
                                        'font-style': 'italic',
                                        'font-weight': 'bold',
                                        'color': 'green'
                                    },
                                    position: { lat: data.last_location.lat, lng: data.last_location.long },
                                }).then((marker: Marker) => {
                                    // if (that.selectedVehicle != undefined) {
                                    marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
                                        .subscribe(e => {
                                            that.liveVehicleName = data.Device_Name;
                                            console.log(e)
                                            // that.drawerHidden = false;
                                            // that.onClickShow = true;
                                        });
                                    // }
                                    that.allData[key].mark = marker;

                                    // marker.setTitle(data.Device_Name);
                                    // marker.showInfoWindow();
                                    var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                    that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, data.iconType, data.status, that);
                                    // that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, 50, 10, that.elementRef);
                                });
                            }
                        }
                        Geocoder.geocode({
                            "position": {
                                lat: data.last_location['lat'],
                                lng: data.last_location['long']
                            }
                        }).then((results: GeocoderResult[]) => {
                            if (results.length == 0) {
                                //not found
                                return null;
                            }
                            let address: any = [
                                results[0].subThoroughfare || "",
                                results[0].thoroughfare || "",
                                results[0].locality || "",
                                results[0].adminArea || "",
                                results[0].postalCode || "",
                                results[0].country || ""
                            ].join(", ");

                            that.address = address;
                        });

                    }
                })(d4)
        })
    }

    liveTrack(map, mark, icons, coords, speed, delay, center, id, iconType, status, that) {
        // liveTrack(map, mark, icons, coords, speed, delay, elementRef) {
        // let that = this;

        var target = 0;
        function _gotoPoint() {
            if (target > coords.length)
                return;

            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;

            console.log("see if getting=> " + mark.getPosition().lat)

            var step = (speed * 1000 * delay) / 3600000;
            if (coords[target] == undefined)
                return;
            var dest = new LatLng(coords[target].lat, coords[target].lng);
            var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); //in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target].lat - lat) / numStep;
            var deltaLng = (coords[target].lng - lng) / numStep;

            function changeMarker(mark, deg) {
                // console.log("deg=> ", deg)
                // map.clear();
                // var imgPath = './assets/imgs/vehicles/' + status.toLowerCase() + iconType + '.png';
                // mark.setIcon(imgPath);
                mark.setRotation(deg);
                // mark.setTitle(data.Device_Name);
                // mark.showInfoWindow();
            }

            function _moveMarker() {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                var head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));

                if (i < distance) {
                    // var head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
                    if ((head != 0) || (head == NaN)) {
                        // icons.rotation = head;
                        changeMarker(mark, head);
                    }
                    mark.setPosition(new LatLng(lat, lng));
                    // if (localStorage.getItem("LiveDevice") != null) {
                    map.setCameraTarget({ lat: lat, lng: lng })
                    // }
                    // if (that.selectedVehicle != undefined) {
                    //     map.setCameraTarget({ lat: lat, lng: lng })
                    // }

                    setTimeout(_moveMarker, delay);
                } else {
                    // var head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
                    if ((head != 0) || (head == NaN)) {
                        // icons.rotation = head;
                        changeMarker(mark, head);
                    }
                    mark.setPosition(dest);
                    // if (localStorage.getItem("LiveDevice") != null) {
                    map.setCameraTarget({ lat: lat, lng: lng })
                    // }
                    // if (that.selectedVehicle != undefined) {
                    //     map.setCameraTarget({ lat: lat, lng: lng })
                    // }

                    target++;
                    setTimeout(_gotoPoint, delay);
                }
            }
            _moveMarker();
        }
        _gotoPoint();
    }

}