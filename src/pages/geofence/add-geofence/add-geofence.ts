import { ApiServiceProvider } from "../../../providers/api-service/api-service";
import { AlertController, ToastController, NavController } from "ionic-angular";
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { GeofencePage } from "../geofence";
declare var google;

@Component({
    selector: 'page-add-geofence',
    templateUrl: './add-geofence.html',
})
export class AddGeofencePage implements OnInit {
    geofenceForm: FormGroup;
    submitAttempt: boolean;
    geofencedetails: any;
    finalcordinate: any;
    islogin: any;
    devicesadd: any;
    curLat: any; curLng: any;
    entering: any;
    exiting: any;
    cord:any;
    isdevice: string;
    constructor(
        public apiCall: ApiServiceProvider,
        public alertCtrl: AlertController,
        public toastCtrl: ToastController,
        public fb: FormBuilder,
        public navCtrl: NavController
    ) {

        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);

        this.geofenceForm = fb.group({
            geofence_name: ['', Validators.required],
            check: [false],
            check1: [false]
        });
    }

    ngOnInit() {
        this.runGeofenceMaps();
    }

    enteringFunc() {
        console.log(this.geofenceForm.value.check);
        this.entering = this.geofenceForm.value.check;
    }

    exitingFunc() {
        console.log(this.geofenceForm.value.check1);
        this.exiting = this.geofenceForm.value.check1;
    }

    disableTap(){
        // var container = document.getElementsByClassName('pac-container');
        // angular.element(container).attr('data-tap-disabled', 'true');
        // var backdrop = document.getElementsByClassName('backdrop');
        // angular.element(backdrop).attr('data-tap-disabled', 'true');
        // angular.element(container).on("click", function () {
        //     document.getElementById('pac-input').blur();
        // });
    }

    creategoefence() {
        debugger;
        this.submitAttempt = true;
        if (this.geofenceForm.valid) {
            if (this.entering || this.exiting) {
                if (this.geofenceForm.value.geofence_name && this.finalcordinate.length) {

                    console.log("add device click")
                    console.log(this.islogin._id);
                    var data = {
                        "uid": this.islogin._id,
                        "geoname": this.geofenceForm.value.geofence_name,
                        "entering": this.entering,
                        "exiting": this.exiting,
                        "geofence": this.finalcordinate
                    }
                    this.apiCall.startLoading().present();
                    this.apiCall.addgeofenceCall(data)
                        .subscribe(data => {
                            this.apiCall.stopLoading();
                            this.devicesadd = data;
                            console.log(this.devicesadd);

                            let toast = this.toastCtrl.create({
                                message: 'Created geofence successfully',
                                position: 'bottom',
                                duration: 2000
                            });

                            toast.onDidDismiss(() => {
                                console.log('Dismissed toast');
                                this.navCtrl.push(GeofencePage);
                            });

                            toast.present();
                        }, err => {
                            this.apiCall.stopLoading();
                        });
                } else {

                    let toast = this.toastCtrl.create({
                        message: 'Select Geofence On Map!',
                        position: 'top',
                        duration: 2000
                    });

                    toast.present();
                }
            } else {
                let alert = this.alertCtrl.create({
                    message: 'All fields are required!',
                    buttons: ['Try Again']
                });
                alert.present();
            }
        }
    }

    runGeofenceMaps() {
        let that=this;

        console.log("get location");
        console.log("Running GEO")

        // navigator.geolocation.getCurrentPosition(onWeatherSuccess1, onWeatherError, { enableHighAccuracy: true });
        var options = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };

        function success(pos) {
            var crd = pos.coords;
            console.log(crd);
            console.log('Your current position is:');
            console.log(`Latitude : ${crd.latitude}`);
            console.log(`Longitude: ${crd.longitude}`);
            console.log(`More or less ${crd.accuracy} meters.`);
            var latLng = new google.maps.LatLng(crd.latitude, crd.longitude);

            var mapOptions = {
                center: latLng,
                disableDefaultUI: true,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("mapGeofence"), mapOptions);
            var input = /** @type {HTMLInputElement} */ (
                document.getElementById('pac-input'));
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);

            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            var infowindow = new google.maps.InfoWindow();
            var marker = new google.maps.Marker({
                map: map
            });
            google.maps.event.addListener(marker, 'click', function () {
                infowindow.open(map, marker);
            });

            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                infowindow.close();
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }

                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);
                }

                // Set the position of the marker using the place ID and location.
                marker.setPlace( /** @type {!google.maps.Place} */({
                    placeId: place.place_id,
                    location: place.geometry.location
                }));
                marker.setVisible(true);
            });
            // var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
            var all_overlays = [];
            var selectedShape;
            var drawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: google.maps.drawing.OverlayType.MARKER,
                drawingControl: true,
                drawingControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT,
                    drawingModes: [
                        // google.maps.drawing.OverlayType.MARKER,
                        // google.maps.drawing.OverlayType.CIRCLE,
                        google.maps.drawing.OverlayType.POLYGON,
                        // google.maps.drawing.OverlayType.RECTANGLE
                    ]
                },
                // markerOptions: {
                //     icon:image
                // },
                circleOptions: {
                    fillColor: '#ffff00',
                    fillOpacity: 0.2,
                    strokeWeight: 3,
                    clickable: false,
                    editable: true,
                    zIndex: 1
                },
                polygonOptions: {
                    clickable: true,
                    draggable: true,
                    editable: true,
                    fillColor: '#ffff00',
                    fillOpacity: 1,

                },
                rectangleOptions: {
                    clickable: true,
                    draggable: true,
                    editable: true,
                    fillColor: '#ffff00',
                    fillOpacity: 0.5,
                }
            });

            function clearSelection() {
                if (selectedShape) {
                    selectedShape.setEditable(false);
                    selectedShape = null;
                }
            }

            function setSelection(shape) {
                clearSelection();
                selectedShape = shape;
                shape.setEditable(true);
                google.maps.event.addListener(selectedShape.getPath(), 'set_at', getPolygonCoords(shape));
            }

            // function deleteSelectedShape() {
            //     if (selectedShape) {
            //         selectedShape.setMap(null);
            //         that.finalcordinate = [];
            //     }
            // }

            // function deleteAllShape() {
            //     for (var i = 0; i < all_overlays.length; i++) {
            //         all_overlays[i].overlay.setMap(null);
            //     }
            //     all_overlays = [];
            // }

            // function CenterControl(controlDiv, map) {

            //     // Set CSS for the control border.
            //     var controlUI = document.createElement('div');
            //     controlUI.style.backgroundColor = '#fff';
            //     controlUI.style.border = '2px solid #fff';
            //     controlUI.style.borderRadius = '3px';
            //     controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
            //     controlUI.style.cursor = 'pointer';
            //     controlUI.style.marginBottom = '22px';
            //     controlUI.style.textAlign = 'center';
            //     controlUI.title = 'Select to delete the shape';
            //     controlDiv.appendChild(controlUI);

            //     // Set CSS for the control interior.
            //     var controlText = document.createElement('div');
            //     controlText.style.color = 'rgb(25,25,25)';
            //     controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
            //     controlText.style.fontSize = '16px';
            //     controlText.style.lineHeight = '38px';
            //     controlText.style.paddingLeft = '5px';
            //     controlText.style.paddingRight = '5px';
            //     controlText.innerHTML = 'Delete Selected Area';
            //     controlUI.appendChild(controlText);

            //     // Setup the click event listeners: simply set the map to Chicago.
            //     controlUI.addEventListener('click', function () {
            //         deleteSelectedShape();
            //         localStorage.removeItem("cordinates");
            //     });

            // }

            drawingManager.setMap(map);
            function getPolygonCoords(newShape) {
                //  console.log("We are one");
                var len = newShape.getPath().getLength();
                that.cord = [];
                that.finalcordinate = [];
                for (var i = 0; i < len; i++) {
                    //  console.log(newShape.getPath().getAt(i).toUrlValue(6));
                    //  console.log(cor)
                    var b = newShape.getPath().getAt(i).toUrlValue(6)
                    console.log(b);
                    var a = [
                    ]

                    a.push(parseFloat(b.split(",")[1]))
                    a.push(parseFloat(b.split(",")[0]))

                    that.cord.push(a);

                }
                if (that.cord.length)
                that.finalcordinate.push(that.cord);
                console.log(that.cord);

                console.log(that.finalcordinate);

                var p = that.cord[0];
                var finalcord = [0];
                that.cord.push(p);
                finalcord.push(that.cord)
                console.log(finalcord);
                var val = JSON.stringify(finalcord);

                //   console.log(val);
                localStorage.setItem('cordinates', val);
                that.isdevice = localStorage.getItem('cordinates');

                /*localStorage.setItem("cordinates", val);*/
            };
            google.maps.event.addListener(drawingManager, 'polygoncomplete', function (event) {

                event.getPath().getLength();
                google.maps.event.addListener(event.getPath(), 'insert_at', function () {
                    var len = event.getPath().getLength();
                    for (var i = 0; i < len; i++) {
                        // console.log(event.getPath().getAt(i).toUrlValue(5));
                    }
                });
                google.maps.event.addListener(event.getPath(), 'set_at', function () {
                    var len = event.getPath().getLength();
                    for (var i = 0; i < len; i++) {
                        //  console.log(event.getPath().getAt(i).toUrlValue(5));
                    }
                });
            });

            google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {

                all_overlays.push(event);
                if (event.type !== google.maps.drawing.OverlayType.MARKER) {
                    drawingManager.setDrawingMode(null);
                    //Write code to select the newly selected object.

                    var newShape = event.overlay;
                    newShape.type = event.type;
                    google.maps.event.addListener(newShape, 'click', function () {
                        setSelection(newShape);
                    });

                    setSelection(newShape);
                }
            });


            

            var centerControlDiv = document.createElement('div');
            // var centerControl = new CenterControl(centerControlDiv, map);

            map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv);
        }

        function error(err) {
            alert(`ERROR(${err.code}): ${err.message}`);
        }

        navigator.geolocation.getCurrentPosition(success, error, options);
    };
}