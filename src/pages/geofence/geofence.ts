import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { GeofenceShowPage } from './geofence-show/geofence-show';
import { AddGeofencePage } from './add-geofence/add-geofence';


@IonicPage()
@Component({
  selector: 'page-geofence',
  templateUrl: 'geofence.html',
})
export class GeofencePage implements OnInit {
  islogin: any;
  setsmsforotp: string;
  isdevice: string;
  devices: any;
  devices1243: any[];
  DeletedDevice: any;
  statusofgeofence: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public toastCtrl: ToastController,
    public alerCtrl: AlertController) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log(this.islogin._id);
    // var login = this.islogin
    this.setsmsforotp = localStorage.getItem('setsms');
    this.isdevice = localStorage.getItem('cordinates');
    console.log("isdevice=> " + this.isdevice);
    console.log("setsmsforotp=> " + this.setsmsforotp);
  }

  ngOnInit() {
    this.getgeofence();
  }

  addgeofence() {
    this.navCtrl.push(AddGeofencePage);
  }


  getgeofence() {
    console.log("getgeofence shape");
    var baseURLp = 'http://51.38.175.41/geofencing/getallgeofence?uid=' + this.islogin._id;

    this.apiCall.startLoading().present();
    this.apiCall.getallgeofenceCall(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.devices1243 = [];
        this.devices = data;
        console.log("devices=> ", this.devices);
        localStorage.setItem('devices', this.devices);
        this.isdevice = localStorage.getItem('devices');
        console.log("isdevices=> ", this.isdevice);
      },
        err => {
          this.apiCall.stopLoading();
          console.log("error => ", err);
        });
  }

  deleteGeo(_id) {
    this.apiCall.startLoading().present();
    var link = 'http://13.126.36.205:3000/geofencing/deletegeofence?id=' + _id;
    this.apiCall.deleteGeoCall(link).
      subscribe(data => {
        this.apiCall.stopLoading();
        this.DeletedDevice = data;
        let toast = this.toastCtrl.create({
          message: 'Deleted Geofence Area successfully.',
          position: 'bottom',
          duration: 2000
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
          this.getgeofence();
        });

        toast.present();
      },
        err => {
          this.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alerCtrl.create({
            title: 'Oops!',
            message: msg.message,
            buttons: ['OK']
          });
          alert.present();
        });
  }

  DelateGeofence(_id) {
    let alert = this.alerCtrl.create({
      message: 'Do you want to delete this geofence area ?',
      buttons: [{
        text: 'No'
      },
      {
        text: 'YES',
        handler: () => {
          this.deleteGeo(_id);
        }
      }]
    });
    alert.present();
  }

  DisplayDataOnMap(item) {
    console.log(item);
    var baseURLp = 'http://51.38.175.41/geofencing/geofencestatus?gid=' + item._id + '&status=' + item.status + '&entering=' + item.entering + '&exiting=' + item.exiting;
    this.apiCall.geofencestatusCall(baseURLp)
      .subscribe(data => {
        this.statusofgeofence = data;
        console.log(this.statusofgeofence);
      },
        err => {
          console.log(err);
        });
  }

  geofenceShow(item) {
    this.navCtrl.push(GeofenceShowPage,{
      param: item
    });
  }
}
