import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginPage } from '../login/login';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { SignupOtp } from './signupOtp';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  signupForm: FormGroup;
  isdealer: boolean;
  submitAttempt: boolean;
  usersignupdetails: any;
  signupUseradd: any;
  responseMessage: string;
  signupDetails: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public apiService: ApiServiceProvider,
    private toastCtrl: ToastController,
    public alertCtrl: AlertController
  ) {
    this.signupForm = formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', Validators.email, Validators.required],
      mobile: ['', Validators.required],
      password: ['', Validators.required],
      confirmpassword: ['', Validators.required],
      check: ['', Validators.required]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  doLogin() {
    this.navCtrl.setRoot(LoginPage);
  }

  IsDealer(check) {
    this.isdealer = check;
  }


  getotp() {
    this.isdealer = false;
    this.submitAttempt = true;
    if (this.signupForm.valid) {
      console.log(this.signupForm.value);
      this.usersignupdetails = this.signupForm.value;
      console.log(this.usersignupdetails);

      localStorage.setItem('usersignupdetails', this.usersignupdetails);
      // $rootScope.signupDetails = store.get('usersignupdetails');
      this.signupDetails = localStorage.getItem("usersignupdetails");

      if (this.signupForm.value.confirmpassword && this.signupForm.value.email && this.signupForm.value.mobile && this.signupForm.value.firstname && this.signupForm.value.lastname && this.signupForm.value.password) {
        if (this.signupForm.value.password == this.signupForm.value.confirmpassword) {

          var usersignupdata = {
            "first_name": this.signupForm.value.firstname,
            "last_name": this.signupForm.value.lastname,
            "email": this.signupForm.value.email,
            "password": this.signupForm.value.password,
            "confirmpass": this.signupForm.value.confirmpassword,
            "org_name": this.signupForm.value.orgname,
            "phone": String(this.signupForm.value.mobile),
            "dealer": this.isdealer
          }
          this.apiService.startLoading();
          this.apiService.signupApi(usersignupdata)
            .subscribe(response => {
              this.apiService.stopLoading();
              this.signupUseradd = response;
              console.log(this.signupUseradd);
              let toast = this.toastCtrl.create({
                message: response.message,
                duration: 2500,
                position: 'top'
              });

              toast.onDidDismiss(() => {
                console.log('Dismissed toast');
                this.navCtrl.push(SignupOtp);
              });

              toast.present();

            }, error => {
              this.apiService.stopLoading();
              if (!error.message) {
                var s = JSON.stringify(error);
                //var s="Error: Duplicate EmailID or Mobile Number"
                if (s.indexOf("Error:") > -1) {
                  let alertPopup = this.alertCtrl.create({
                    title: 'Signup failed!',
                    message: s.split(":")[1],
                    buttons: ['Ok']
                  });
                  alertPopup.present();
                }
              }
              else {
                let alertPopup = this.alertCtrl.create({
                  title: 'Signup failed!',
                  message: error.message
                });
                alertPopup.present();
              }
              this.responseMessage = "Something Wrong";
            })
        } else {
          let alertPopup = this.alertCtrl.create({
            title: 'Warning!',
            message: "Password and Confirm Password Not Matched",
            buttons: ['OK']
          });
          alertPopup.present();
        }
      }
    }
  }

}
