import { NavParams, ViewController, ToastController, AlertController } from "ionic-angular";
import { Component } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ApiServiceProvider } from "../../../providers/api-service/api-service";
declare var google;

@Component({
    selector: 'page-edit-route-details',
    templateUrl: './edit-route-details.html'
})
export class EditRouteDetailsPage {
    editrouteForm: FormGroup;
    submitAttempt: boolean;
    coordinate: any;
    finlCoords: any;
    islogin: any;
    editdata: any;
    params: any;
    // polyArray: any = [];

    constructor(
        navParam: NavParams,
        public viewCtrl: ViewController,
        public toastCtrl: ToastController,
        public fb: FormBuilder,
        public apiCall: ApiServiceProvider,
        public alertCtrl: AlertController) {

        this.params = navParam.get("param");
        console.log("params=> ", this.params)
        this.editrouteForm = fb.group({
            source: [this.params.source, Validators.required],
            destination: [this.params.destination, Validators.required],
            route: [this.params.name, Validators.required]
        });
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log(this.islogin._id);
    }
    dismiss() {
        this.viewCtrl.dismiss();
    }

    getRouteEdit() {
        // $rootScope.routeid = dvDistance;
        // console.log($rootScope.routeid);
        var renderer = new google.maps.DirectionsRenderer({
            suppressPolylines: true
        });
        // var im = 'http://www.robotwoods.com/dev/misc/bluecircle.png';

        var options = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };

        function error(err) {
            alert(`ERROR(${err.code}): ${err.message}`);
        }

        function success(pos) {
            var crd = pos.coords;
            // var image = 'https://maps.google.com/mapfiles/ms/micons/blue.png';
            var myLatLng = new google.maps.LatLng(crd.latitude, crd.longitude);
            var map = new google.maps.Map(document.getElementById('dvMap'), {
                center: myLatLng,
                zoom: 11,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                draggable: true
            });
            var directionsDisplay = new google.maps.DirectionsRenderer(renderer);
            var directionsService = new google.maps.DirectionsService();
            directionsDisplay.setMap(map);
            directionsDisplay.setPanel(document.getElementById('dvPanel'));

            // var userMarker = new google.maps.Marker({
            //     position: myLatLng,
            //     map: map,
            //     icon: im
            // });
            //*********DIRECTIONS AND ROUTE**********************//
            var source = (<HTMLInputElement>document.getElementById("txtSource")).value;
            var destination = (<HTMLInputElement>document.getElementById("txtDestination")).value;

            console.log("source=> ", source)
            console.log("destination=> ", destination)

            var polyArray = [];
            directionsService.route({
                origin: source,
                destination: destination,
                travelMode: 'DRIVING',
                provideRouteAlternatives: true
            }, function (response, status) {
                console.log(response);
                if (status === 'OK') {
                    renderer.setDirections(response);
                    renderer.setMap(map);
                    renderer.getRouteIndex();

                    for (var j = 0; j < response.routes.length; j++) {
                        var path = new google.maps.MVCArray();

                        polyArray.push(new google.maps.Polyline({
                            map: map,
                            strokeColor: "#615959",
                            strokeOpacity: 0.3,
                            strokeWeight: 5
                        }));
                        if (j == 0) polyArray[0].setOptions({
                            strokeColor: '#1E90FF',
                            strokeOpacity: 1.0
                        });
                        polyArray[polyArray.length - 1].setPath(path);
                        polyArray[polyArray.length - 1].getPath();

                        google.maps.event.addListener(polyArray[polyArray.length - 1], 'click', function (e) {
                            for (var i = 0; i < polyArray.length; i++) {
                                polyArray[i].setOptions({
                                    strokeOpacity: 0.3,
                                    strokeColor: "#615959"
                                });
                            }
                            this.setOptions({
                                strokeOpacity: 1.0,
                                strokeColor: "#1E90FF"
                            });
                            var s = this.getPath().getArray().toString();
                            var arr = [];
                            var arrNew = [];
                            var sNew = s.split("),(");
                            for (var k = 0; k < sNew.length; k++) {
                                if (k % 2 == 1) {
                                    arrNew.push(sNew[k].split(","));
                                    arr.length = 0;
                                }
                            }

                            console.log(arrNew);

                            this.finlCoords = [];
                            for (var m = 0; m < arrNew.length; m++) {
                                var lat = arrNew[m][0];
                                var latitude = parseFloat(lat);

                                var lang = arrNew[m][1];
                                var longitude = parseFloat(lang);

                                this.finlCoords.push([longitude, latitude]);
                            }
                            console.log(this.finlCoords);
                        })
                        for (var i = 0, len = response.routes[j].overview_path.length; i < len; i++) {
                            path.push(response.routes[j].overview_path[i]);
                        }
                    }
                } else {
                    console.log('Directions request failed due to ' + status);
                }
            });
            //*********DISTANCE AND DURATION**********************//
            var service = new google.maps.DistanceMatrixService();
            service.getDistanceMatrix({
                origins: [source],
                destinations: [destination],
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC,

                avoidHighways: true,
                avoidTolls: true
            }, function (response, status) {
                console.log(response);
                if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS" && status === google.maps.DirectionsStatus.OK) {
                    var distance = response.rows[0].elements[0].distance.text;
                    var duration = response.rows[0].elements[0].duration.text;
                    console.log(distance);
                    console.log(duration);


                } else {
                    alert("Unable to find the distance via road.");
                }
            });
        }
        navigator.geolocation.getCurrentPosition(success, error, options);
    }

    editRoute() {
        this.submitAttempt = true;
        if (this.editrouteForm.valid) {
            console.log(this.editrouteForm.value);
            // console.log("edit form id=> "+ this.editrouteForm.value._id);
            this.coordinate = this.finlCoords;
            /* console.log($rootScope.routesdetail);*/
            var devicedetail = {
                "source": this.editrouteForm.value.source,
                "destination": this.editrouteForm.value.destination,
                "name": this.editrouteForm.value.name,
                "user": this.islogin._id,
                "loc": this.coordinate,
                "_id": this.params._id
            }
            // var link = 'http://13.126.36.205:3000/trackRoute/' + this.editrouteForm.value._id;
            this.apiCall.startLoading().present();
            this.apiCall.gettrackRouteCall(this.params._id,devicedetail)
                .subscribe(data => {
                    this.apiCall.stopLoading();
                    this.editdata = data;
                    console.log("editdata=> " + this.editdata);
                    let toast = this.toastCtrl.create({
                        message: 'Route was updated successfully',
                        position: 'bottom',
                        duration: 1500
                    });

                    toast.onDidDismiss(() => {
                        console.log('Dismissed toast');
                        this.viewCtrl.dismiss(this.editdata);
                    });

                    toast.present();
                },
                    err => {
                        this.apiCall.stopLoading();
                        console.log("error in route update=> ", err)
                        // var body = err._body;
                        // var msg = JSON.parse(body);
                        // let alert = this.alertCtrl.create({
                        //     title: 'Oops!',
                        //     message: msg.message,
                        //     buttons: ['OK']
                        // });
                        // alert.present();
                    });
        }
    }

}