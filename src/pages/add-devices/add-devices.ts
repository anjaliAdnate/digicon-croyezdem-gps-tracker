import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, ModalController, PopoverController, ViewController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { HistoryDevicePage } from '../history-device/history-device';
import { SMS } from '@ionic-native/sms';
import { AddDeviceModalPage } from '../customers/modals/add-device-modal';
import { UpdateDevicePage } from './update-device/update-device';
import { LivePage } from '../live/live';
declare var google;

@IonicPage()
@Component({
  selector: 'page-add-devices',
  templateUrl: 'add-devices.html',
})
export class AddDevicesPage implements OnInit {

  islogin: any;
  isDealer: any;
  islogindealer: string;
  stausdevice: string;
  device_types: { vehicle: string; name: string; }[];
  GroupType: { vehicle: string; name: string; }[];
  GroupStatus: { name: string; }[];
  allDevices: any;
  allDevicesSearch: any = [];

  socket: any;
  isdevice: string;
  userPermission: string;
  option_switch: string;
  dataEngine: any;
  DeviceConfigStatus: any;
  messages: string;
  editdata: any;
  responseMessage: string;
  searchCountryString = ''; // initialize your searchCountryString string empty
  countries: any;
  latLang: any;

  @ViewChild('popoverContent', { read: ElementRef }) content: ElementRef;
  @ViewChild('popoverText', { read: ElementRef }) text: ElementRef;
  page: number = 0;
  limit: number = 5;
  ndata: any;
  searchItems: any;
  searchQuery: string = '';
  items: any[];


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    private sms: SMS,
    public modalCtrl: ModalController,
    public popoverCtrl: PopoverController
  ) {



    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("islogin devices => " + JSON.stringify(this.islogin));
    this.isDealer = this.islogin.isDealer
    console.log("isDealer=> " + this.isDealer);

    this.islogindealer = localStorage.getItem('isDealervalue');
    console.log("islogindealer=> " + this.islogindealer);

    if (navParams.get("label") && navParams.get("value")) {
      console.log("lebel=> ", navParams.get("label"))
      console.log("value=> ", navParams.get("value"))
      this.stausdevice = localStorage.getItem('status');
      console.log("stausdevice=> " + this.stausdevice);
    } else {
      // localStorage.removeItem("status");
      this.stausdevice = undefined;
    }




  }

  ngOnInit() {
    this.getdevices();
    console.log(this.isDealer);
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.getdevices();
    refresher.complete();
  }

  shareVehicle(d_data) {
    let that = this;
    console.log("share")
    const prompt = this.alertCtrl.create({
      title: 'Share Vehicle',
      // message: "Enter a name for this new album you're so keen on adding",
      inputs: [
        {
          name: 'device_name',
          value: d_data.Device_Name
        },
        {
          name: 'shareId',
          placeholder: 'Enter Email Id/Mobile Number'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Share',
          handler: data => {
            console.log('Saved clicked');
            console.log("clicked=> ", data)

            that.sharedevices(data, d_data)

          }
        }
      ]
    });
    prompt.present();
  }

  sharedevices(data, d_data) {
    let that = this;
    console.log("shareId=> " + data.shareId);
    console.log("d_data=>", d_data)
    var devicedetails = {
      "did": d_data._id,
      "email": data.shareId,
      "uid": that.islogin._id
    }

    that.apiCall.startLoading().present();
    that.apiCall.deviceShareCall(devicedetails)
      .subscribe(data => {
        that.apiCall.stopLoading();
        var editdata = data;
        console.log(editdata);
        let toast = that.toastCtrl.create({
          message: data.message,
          position: 'bottom',
          duration: 2000
        });

        // toast.onDidDismiss(() => {
        //   console.log('Dismissed toast');

        // });

        toast.present();
      },
        err => {
          that.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            title: 'Oops!',
            message: msg.message,
            buttons: ['OK']
          });
          alert.present();
        });
  }

  presentPopover(ev, data) {
    console.log("populated=> " + JSON.stringify(data))
    let popover = this.popoverCtrl.create(PopoverPage,
      {
        vehData: data
      },
      {
        cssClass: 'contact-popover'
      });

    popover.onDidDismiss(() => {
      this.getdevices();
    })

    popover.present({
      ev: ev
    });
  }

  doInfinite(infiniteScroll) {
    let that = this;
    that.page = that.page + 1;
    setTimeout(() => {
      var baseURLp;
      if (that.stausdevice) {
        baseURLp = 'http://51.38.175.41/devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&statuss=' + that.stausdevice + '&skip=' + that.page + '&limit=' + that.limit;
      }
      else {
        baseURLp = 'http://51.38.175.41/devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&skip=' + that.page + '&limit=' + that.limit;
      }
      that.apiCall.getdevicesApi(baseURLp)
        .subscribe(
          res => {
            that.ndata = res.devices;

            for (let i = 0; i < that.ndata.length; i++) {
              that.allDevices.push(that.ndata[i]);
              // that.allDevicesSearch.push(that.ndata[i]);
            }
            that.allDevicesSearch = that.allDevices;
            console.log("all devices => " + that.allDevices)
          },
          error => {
            console.log(error);
          });

      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }

  getdevices() {
    // debugger;
    var baseURLp;
    if (this.stausdevice) {
      baseURLp = 'http://51.38.175.41/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.stausdevice + '&skip=' + this.page + '&limit=' + this.limit;

    }
    else {
      baseURLp = 'http://51.38.175.41/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
    }
    console.log("getdevices");
    this.apiCall.startLoading().present();
    this.apiCall.getdevicesApi(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.ndata = data.devices;
        this.allDevices = this.ndata;
        this.allDevicesSearch = this.ndata;
        //this.initializeItems();
        // this.searchItems = this.allDevices.map(function (d) {
        //   return { Device_Name: d.Device_Name }
        // })
        console.log(" devices=> " + data.devices);
        console.log("data devices=> " + data.devices.length);
      },
        err => {
          console.log("error=> ", err);
          this.apiCall.stopLoading();
        });
  }

  livetrack(device) {
    localStorage.setItem("LiveDevice", "LiveDevice");
    this.navCtrl.push(LivePage, {
      device: device
    })
    // this.navCtrl.push(LiveSingleDevice, {
    //   param: device
    // })
  }

  showHistoryDetail(device) {
    this.navCtrl.push(HistoryDevicePage, {
      device: device
    })
  }

  device_address(device, index) {
    let that = this;
    // debugger;
    // console.log("device address=> "+ JSON.stringify(device))
    that.userPermission = localStorage.getItem('condition_chk');
    that.option_switch = that.userPermission;
    console.log(that.option_switch);
    // console.log(device)
    console.log("abc " + device.last_location);
    that.allDevices[index].address = "N/A";
    if (!device.last_location) {
      // device.address = N/A
      that.allDevices[index].address = "N/A";
      console.log(that.allDevices[index]);
      console.log("inside undefined function");

    } else if (device.last_location) {
      console.log("aaa", device.last_location);
      console.log("inside latlong function")
      var lattitude = device.last_location.lat;
      var longitude = device.last_location.long;
      console.log(lattitude);
      console.log(longitude);

      var geocoder = new google.maps.Geocoder;
      var latlng = new google.maps.LatLng(lattitude, longitude);

      var request = {
        "latLng": latlng
      };

      geocoder.geocode(request, function (resp, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          //console.log("add data=> " + JSON.stringify(resp))
          if (resp[0] != null) {
            console.log("address is: " + resp[0].formatted_address);
            console.log("index=> ", index)
            that.allDevices[index].address = resp[0].formatted_address;
            console.log(that.allDevices[index])
          } else {
            console.log("No address available")
          }
        }
        else {
          // that.allDevices[index].address = device.last_location.lat + ' ,' + device.last_location.long
          that.allDevices[index].address = 'N/A';

        }
      })
    }
    // console.log(JSON.stringify(this.allDevices))
  }

  IgnitionOnOff(d) {
    this.messages = undefined;
    console.log("d=> " + d);
    this.dataEngine = d;
    console.log("dataEngine=> " + this.dataEngine);
    console.log("simnumber=> " + d.sim_number);
    console.log("device_type=> " + this.dataEngine.device_model.device_type);
    console.log("engine_status=> " + this.dataEngine.engine_status);

    var baseURLp = 'http://51.38.175.41/deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;

    this.apiCall.startLoading().present();
    this.apiCall.ignitionoffCall(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.DeviceConfigStatus = data;
        console.log("immoblizer_command=> " + this.DeviceConfigStatus[0].immoblizer_command);
      },
        error => {
          this.apiCall.stopLoading();
          console.log(error);
        });


    if (this.dataEngine.ignitionLock == '1') {
      this.messages = 'Do you want to unlock the engine?'
    } else {
      if (this.dataEngine.ignitionLock == '0') {
        this.messages = 'Do you want to lock the engine?'
      }
    }

    let alert = this.alertCtrl.create({
      message: this.messages,
      buttons: [{
        text: 'YES',
        handler: () => {
          var devicedetail = {
            "_id": this.dataEngine._id,
            "engine_status": !this.dataEngine.engine_status
          }
          this.apiCall.startLoading().present();
          this.apiCall.deviceupdateCall(devicedetail)
            .subscribe(response => {
              this.apiCall.stopLoading();
              this.editdata = response;
              console.log(this.editdata);
              const toast = this.toastCtrl.create({
                message: response.message,
                duration: 2000,
                position: 'top'
              });
              toast.present();
              this.responseMessage = "Edit successfully";
              this.getdevices();
              console.log(this.responseMessage);

              var msg;
              if (!this.dataEngine.engine_status) {
                msg = this.DeviceConfigStatus[0].resume_command;
              }
              else {
                msg = this.DeviceConfigStatus[0].immoblizer_command;
              }
              // console.log(this.DeviceConfigStatus[0].immoblizer_command);
              // Send a text message using default options
              // this.sms.send('416123456', 'Hello world!');
              this.sms.send(d.sim_number, msg);
              const toast1 = this.toastCtrl.create({
                message: 'SMS sent successfully',
                duration: 2000,
                position: 'bottom'
              });
              toast1.present();
            },
              error => {
                this.apiCall.stopLoading();
                console.log(error);
              });

        }
      },
      {
        text: 'No'
      }]
    });
    alert.present();
  };

  dialNumber(number) {
    window.open('tel:' + number, '_system');
  }

  getItems(ev: any) {
    console.log(ev.target.value, this.allDevices)
    const val = ev.target.value.trim();
    this.allDevicesSearch = this.allDevices.filter((item) => {
      return (item.Device_Name.toLowerCase().indexOf(val.toLowerCase()) > -1);
    });
    console.log("search====", this.allDevicesSearch);
  }


  onCancel(ev) {
    // Reset the field

    this.getdevices();
    // ev.target.value = '';
  }

  openAdddeviceModal() {
    let profileModal = this.modalCtrl.create(AddDeviceModalPage);
    profileModal.onDidDismiss(data => {
      console.log(data);
      this.getdevices();
    });
    profileModal.present();
  }
}


@Component({
  template: `
  
    <ion-list>
      <ion-item class="text-palatino" (click)="editItem()">
        <ion-icon name="create"></ion-icon>&nbsp;&nbsp;Edit
      </ion-item>
      <ion-item class="text-san-francisco" (click)="deleteItem()">
      <ion-icon name="trash"></ion-icon>&nbsp;&nbsp;Delete
      </ion-item>
      <ion-item class="text-seravek" (click)="shareItem()">
      <ion-icon name="share"></ion-icon>&nbsp;&nbsp;Share
      </ion-item>
    </ion-list>
  `
})
export class PopoverPage implements OnInit {
  contentEle: any;
  textEle: any;
  vehData: any;
  islogin: any;
  constructor(
    navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public apiCall: ApiServiceProvider,
    public toastCtrl: ToastController,
    public navCtrl: NavController,
    public viewCtrl: ViewController
  ) {
    this.vehData = navParams.get("vehData");
    console.log("popover data=> ", this.vehData);

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("islogin devices => " + this.islogin);
  }

  ngOnInit() {
    // if (this.navParams.data) {
    //   this.contentEle = this.navParams.data.contentEle;
    //   this.textEle = this.navParams.data.textEle;

    // }
  }

  editItem() {
    console.log("edit")
    let modal = this.modalCtrl.create(UpdateDevicePage, {
      vehData: this.vehData
    });
    modal.onDidDismiss(() => {
      console.log("modal dismissed!")
      this.viewCtrl.dismiss();
    })
    modal.present();
  }

  deleteItem() {
    let that = this;
    console.log("delete")
    let alert = this.alertCtrl.create({
      message: 'Do you want to delete this vehicle ?',
      buttons: [{
        text: 'YES PROCEED',
        handler: () => {
          console.log(that.vehData.Device_ID)
          that.deleteDevice(that.vehData.Device_ID);
        }
      },
      {
        text: 'NO'
      }]
    });
    alert.present();
  }

  deleteDevice(d_id) {
    this.apiCall.startLoading().present();
    this.apiCall.deleteDeviceCall(d_id)
      .subscribe(data => {
        this.apiCall.stopLoading();
        var DeletedDevice = data;
        console.log(DeletedDevice);

        let toast = this.toastCtrl.create({
          message: 'Vehicle deleted successfully!',
          position: 'bottom',
          duration: 2000
        });

        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
          // this.navCtrl.push(AddDevicesPage);
          this.viewCtrl.dismiss();
        });

        toast.present();
      },
        err => {
          this.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            title: 'Oops!',
            message: msg.message,
            buttons: ['OK']
          });
          alert.present();
        });
  }

  shareItem() {
    let that = this;
    console.log("share")
    const prompt = this.alertCtrl.create({
      title: 'Share Vehicle',
      // message: "Enter a name for this new album you're so keen on adding",
      inputs: [
        {
          name: 'device_name',
          value: that.vehData.Device_Name
        },
        {
          name: 'shareId',
          placeholder: 'Enter Email Id/Mobile Number'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Share',
          handler: data => {
            console.log('Saved clicked');
            console.log("clicked=> ", data)

            that.sharedevices(data)

          }
        }
      ]
    });
    prompt.present();
  }

  sharedevices(data) {
    let that = this;
    console.log(data.shareId);
    var devicedetails = {
      "did": that.vehData._id,
      "email": data.shareId,
      "uid": that.islogin._id
    }

    that.apiCall.startLoading().present();
    that.apiCall.deviceShareCall(devicedetails)
      .subscribe(data => {
        that.apiCall.stopLoading();
        var editdata = data;
        console.log(editdata);
        let toast = that.toastCtrl.create({
          message: data.message,
          position: 'bottom',
          duration: 2000
        });

        // toast.onDidDismiss(() => {
        //   console.log('Dismissed toast');

        // });

        toast.present();
      },
        err => {
          that.apiCall.stopLoading();
          var body = err._body;
          var msg = JSON.parse(body);
          let alert = this.alertCtrl.create({
            title: 'Oops!',
            message: msg.message,
            buttons: ['OK']
          });
          alert.present();
        });
  }
}
