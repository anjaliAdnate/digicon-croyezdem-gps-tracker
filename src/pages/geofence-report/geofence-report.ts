import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
// import * as moment from 'moment';


@IonicPage()
@Component({
  selector: 'page-geofence-report',
  templateUrl: 'geofence-report.html',
})
export class GeofenceReportPage implements OnInit {


  geofenceRepoert: any;
  Ignitiondevice_id: any;
  datetimeEnd: string;
  datetimeStart: string;
  islogin: any;
  devices1243: any[];
  geofencelist: any;
  devicesReport: any;
  geofencedata: any[];
  StartTime: string;
  Startetime: string;
  Startdate: string;
  datetime: number;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicallGeofenceReport: ApiServiceProvider,
    public alertCtrl: AlertController) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);
  //   var temp = new Date();

  //  var temp = new Date();
  //   this.datetimeStart = temp.toISOString();
  //   // $rootScope.datetimeStart.setHours(0,0,0,0);
  //   this.datetimeEnd = temp.toISOString();
  var temp = new Date();
    
    // this.datetimeStart = temp.toISOString();
    var settime= temp.getTime();
    // this.datetime=new Date(settime).setMinutes(0);
    this.datetime=new Date(settime).setHours(5,30,0);

     this.datetimeStart =new Date(this.datetime).toISOString();
     
     var a= new Date()
    a.setHours(a.getHours() + 5);
    a.setMinutes(a.getMinutes()+30);
    this.datetimeEnd = new Date(a).toISOString();
  }

  ngOnInit() {
    this.getgeofence1();
  }

  getgeofence1() {
    console.log("getgeofence shape");

    var baseURLp = 'http://51.38.175.41/geofencing/getallgeofence?uid=' + this.islogin._id;
    this.apicallGeofenceReport.startLoading().present();
    this.apicallGeofenceReport.getallgeofenceCall(baseURLp)
      .subscribe(data => {
        this.apicallGeofenceReport.stopLoading();
        this.devices1243 = [];
        this.geofencelist = data;
        console.log("geofencelist=> ", this.geofencelist)

      },
        err => {
          this.apicallGeofenceReport.stopLoading();
          console.log(err)
        });
  }

  getGeofencedata(from, to, geofence) {
    console.log("selectedVehicle=> ", geofence)
    this.Ignitiondevice_id = geofence._id;
    this.getGeofenceReport(from, to);
  }


  change(datetimeStart) {
    console.log(datetimeStart);
  }

  change1(datetimeEnd) {
    console.log(datetimeEnd);
  }


  getGeofenceReport(starttime, endtime) {
    // console.log("this.Ignitiondevice_id=> "+ this.Ignitiondevice_id)
    // if (this.Ignitiondevice_id == undefined) {
    //   this.Ignitiondevice_id = "";
    // }
    console.log(starttime);
    console.log(endtime);

    this.apicallGeofenceReport.startLoading().present();
    this.apicallGeofenceReport.getGeogenceReportApi(starttime, endtime, this.Ignitiondevice_id, this.islogin._id)
      .subscribe(data => {
        this.apicallGeofenceReport.stopLoading();

        this.geofenceRepoert = data;
        console.log(this.geofenceRepoert);

        if(this.geofenceRepoert.length==0){
          let alert = this.alertCtrl.create({
            message: "No Data Found",
            buttons: ['OK']
          });
          alert.present();
        }
        // this.devicesReport = data;
        // console.log(this.devicesReport);
        // this.geofencedata = [];
        // if (this.devicesReport.length == 0) {
        //   var alertPopup = this.alertCtrl.create({
        //     message: 'No Data Found',
        //     buttons: ['OK']
        //   });
        //   alertPopup.present();

        // } else {
        //   for (var i = 0; i < this.devicesReport.length; i++) {
        //     this.StartTime = JSON.stringify(this.devicesReport[i].timestamp).split('"')[1].split('T')[0];
        //     var gmtDateTime = moment.utc(JSON.stringify(this.devicesReport[i].timestamp).split('T')[1].split('.')[0], "HH:mm:ss");
        //     var gmtDate = moment.utc(JSON.stringify(this.devicesReport[i].timestamp).slice(0, -1).split('T'), "YYYY-MM-DD");
        //     this.Startetime = gmtDateTime.local().format(' h:mm a');
        //     this.Startdate = gmtDate.format('ll');

        //     this.geofencedata.push({ 'vehicleName': this.devicesReport[i].vehicleName, 'time': this.Startetime, 'date': this.Startdate, 'direction': this.devicesReport[i].direction, 'address': this.devicesReport[i].address });
        //   }

        //   console.log(this.geofencedata);
        // }
      }, error => {
        this.apicallGeofenceReport.stopLoading();
        console.log(error);
        
          // let alert = this.alertCtrl.create({
          //   message: "Please Select Geofencing",
          //   buttons: ['OK']
          // });
          // alert.present();
        
        // let alert = this.alertCtrl.create({
        //   message: 'No data found!',
        //   buttons: ['OK']
        // });
        // alert.present();
      });
  }

}
