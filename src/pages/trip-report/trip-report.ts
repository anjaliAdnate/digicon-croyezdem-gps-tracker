import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-trip-report',
  templateUrl: 'trip-report.html',
})
export class TripReportPage implements OnInit {
  islogin: any;
  devices1243: any[];
  devices: any;
  portstemp: any;
  isdevice: string;
  device_id: any;
  isdeviceTripreport: string;
  datetimeStart: string;
  datetimeEnd: string;
  TripReportData: any[];
  TripsdataAddress: any[];
  deviceId: any;
  distanceBt: number;
  StartTime: string;
  Startetime: string;
  Startdate: string;
  EndTime: string;
  Endtime: string;
  Enddate: string;
  datetime: number;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicalligi: ApiServiceProvider,
    public alertCtrl: AlertController) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);
    // var temp = new Date();
    // this.datetimeStart = temp.toISOString();
    // // $rootScope.datetimeStart.setHours(0,0,0,0);
    // this.datetimeEnd = temp.toISOString();
    var temp = new Date();
    
    // this.datetimeStart = temp.toISOString();
    var settime= temp.getTime();
    // this.datetime=new Date(settime).setMinutes(0);
    this.datetime=new Date(settime).setHours(5,30,0);

     this.datetimeStart =new Date(this.datetime).toISOString();
     
     var a= new Date()
    a.setHours(a.getHours() + 5);
    a.setMinutes(a.getMinutes()+30);
    this.datetimeEnd = new Date(a).toISOString();
  }

  ngOnInit() {
    this.getdevices();
  }

  getdevices() {
    this.apicalligi.startLoading().present();
    this.apicalligi.livedatacall(this.islogin._id, this.islogin.email)
      .subscribe(data => {
        this.apicalligi.stopLoading();
        this.devices1243 = [];
        this.devices = data;
        this.portstemp = data.devices;
        this.devices1243.push(data);
        console.log(this.devices1243);
        localStorage.setItem('devices1243', JSON.stringify(this.devices1243));
        this.isdevice = localStorage.getItem('devices1243');
        for (var i = 0; i < this.devices1243[i]; i++) {
          this.devices1243[i] = {
            'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
          };
        }
      },
        err => {
          this.apicalligi.stopLoading();
          console.log(err);
        });
  }

  getTripdevice(from, to, item) {
    console.log(item);
    this.device_id = item._id;
    console.log(this.device_id);

    localStorage.setItem('devices_id', item);
    this.isdeviceTripreport = localStorage.getItem('devices_id');
  }

  change(datetimeStart) {
    console.log(datetimeStart);
  }

  change1(datetimeEnd) {
    console.log(datetimeEnd);
  }

  getTripReport(starttime, endtime) {
    this.TripReportData = [];

    console.log("trip Report report");
    console.log(starttime);
    console.log(endtime);

    if (endtime <= starttime && this.device_id) {
      console.log("to time always greater");
      let alert = this.alertCtrl.create({
        message: 'To time always greater than From Time',
        buttons: ['OK']
      });
      alert.present();
    } else {

      var baseURLp = 'http://51.38.175.41/user_trip/trip_detail?uId=' + this.islogin._id + '&from_date=' + starttime + '&to_date=' + endtime + '&device=' + this.device_id;
      this.apicalligi.startLoading().present();
      this.apicalligi.trip_detailCall(baseURLp)
        .subscribe(data => {

          this.apicalligi.stopLoading();
          if (data.length == 0) {
            let alert = this.alertCtrl.create({
              message: 'No data found',
              buttons: ['OK']
            });
            alert.present();
          } else {
            console.log("response=> "+data);
            this.TripsdataAddress = [];

            for (var i = 0; i < data.length; i++) {

              this.deviceId = data[i]._id;
              this.distanceBt = data[i].distance / 1000;
            
              this.StartTime = JSON.stringify(data[i].start_time).split('"')[1].split('T')[0];
              var gmtDateTime = moment.utc(JSON.stringify(data[i].start_time).split('T')[1].split('.')[0], "HH:mm:ss");
              var gmtDate = moment.utc(JSON.stringify(data[i].start_time).slice(0, -1).split('T'), "YYYY-MM-DD");
              this.Startetime = gmtDateTime.local().format(' h:mm a');
              this.Startdate = gmtDate.format('ll');

              this.EndTime = JSON.stringify(data[i].end_time).split('"')[1].split('T')[0];
              var gmtDateTime1 = moment.utc(JSON.stringify(data[i].end_time).split('T')[1].split('.')[0], "HH:mm:ss");
              var gmtDate1 = moment.utc(JSON.stringify(data[i].end_time).slice(0, -1).split('T'), "YYYY-MM-DD");
              this.Endtime = gmtDateTime1.local().format(' h:mm a');
              this.Enddate = gmtDate1.format('ll');
              // var arr = [];

              this.TripReportData.push({ 'Startetime': this.Startetime, 'Startdate': this.Startdate, 'Endtime': this.Endtime, 'Enddate': this.Enddate, 'distance': this.distanceBt, '_id': this.deviceId, 'startAddress': data[i].startAddress, 'endAddress': data[i].endAddress, 'start_time': data[i].start_time, 'end_time': data[i].end_time });

            }
            console.log("TripReportData=> "+ this.TripReportData);
          }
        },
          err => {
            this.apicalligi.stopLoading();
            console.log(err);
          });

    }
  }

}
