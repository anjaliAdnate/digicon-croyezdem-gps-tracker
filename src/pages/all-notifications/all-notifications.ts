import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, PopoverController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
// import * as moment from 'moment';
import * as io from 'socket.io-client';   // with ES6 import
import { FilterPage } from './filter/filter';
// import { OrderByPipe } from 'fuel-ui/fuel-ui';

@IonicPage()
@Component({
  selector: 'page-all-notifications',
  templateUrl: 'all-notifications.html',
})
export class AllNotificationsPage implements OnInit {
  islogin: any;
  NotifyData: any = [];
  MassArray1: any = [];
  notiftime: any;
  time: string;
  date: string;
  socket: any;
  page: number = 1;

  items = [];
  limit: number = 15;
  ndata: any;
  portstemp: any;
  keyData: any = [];
  dates: any;
  selectedVehicle: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public events: Events,
    public popoverCtrl: PopoverController) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.events.publish('cart:updated', 0);

    for (let i = 0; i < 30; i++) {
      this.items.push(this.items.length);
    }
  }

  ngOnInit() {
    localStorage.removeItem("filterByType");
    localStorage.removeItem("filterByDate");

    this.getUsersOnScroll();
    this.getVehicleList();

    this.socket = io('https://www.oneqlik.in/sbNotifIO', {
      transports: ['websocket', 'polling']
    });

    this.socket.on('connect', () => {
      console.log('IO Connected page');
      console.log("socket connected page ", this.socket.connected)
    });

    this.socket.on(this.islogin._id, (msg) => {
      // console.log("tab push notify msg=> " + JSON.stringify(msg))
      this.NotifyData.push(msg);
      // this.events.publish('cart:updated', ++this.count);

      console.log("tab notice data=> " + this.NotifyData)
    });
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.getUsersOnScroll();
    refresher.complete();
  }

  filterby(ev) {
    // console.log("populated=> " + JSON.stringify(data))
    let popover = this.popoverCtrl.create(FilterPage,
      {

        cssClass: 'iosPop-popover'

      });

    popover.present({
      ev: ev
    });

    popover.onDidDismiss(data => {
      // console.log("from popover=> " + JSON.stringify(data))
      if (localStorage.getItem("types") != null) {
        var typeArr = [];
        if (data != null) {
          if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
              typeArr.push(data[i].filterValue)
            }
          }
          this.keyData = typeArr;
          this.filterByType();
        }
      } else {
        if (localStorage.getItem("dates") != null) {
          // console.log("dates=> ", data)
          this.dates = data;
          this.filterByDate()
        }
      }

    })
  }

  filterByType() {
    let that = this;
    console.log(that.keyData)
    localStorage.setItem("filterByType", "filterByType");
    that.apiCall.startLoading().present();
    that.apiCall.filterByType(that.islogin._id, that.page, that.limit, that.keyData)
      .subscribe(data => {
        that.apiCall.stopLoading();
        that.ndata = data;
        that.MassArray1 = that.ndata;
        localStorage.removeItem("types");
      },
        err => {
          that.apiCall.stopLoading();
          console.log(err)
        });
  }

  filterByDate() {
    let that = this;
    localStorage.setItem("filterByDate", "filterByDate");
    that.apiCall.startLoading().present();
    that.apiCall.filterByDateCall(that.islogin._id, that.page, that.limit, that.dates)
      .subscribe(data => {
        that.apiCall.stopLoading();
        that.ndata = data;
        that.MassArray1 = that.ndata;
        localStorage.removeItem("dates");
      },
        err => {
          that.apiCall.stopLoading();
          console.log(err)
        });
  }

  temp(key) {
    console.log(key)
    this.apiCall.startLoading().present();
    this.apiCall.getFilteredcall(this.islogin._id, this.page, this.limit, key.Device_ID)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.ndata = data;
        this.MassArray1 = this.ndata;
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err)
        });
  }

  getUsersOnScroll() {
    // this.apiCall.startLoading().present();
    this.apiCall.getDataOnScroll(this.islogin._id, this.page, this.limit)
      .subscribe(
        res => {
          // this.apiCall.stopLoading();
          // console.log("res=> " + res)
          this.ndata = res;
          this.MassArray1 = this.ndata;
        },
        error => {
          // this.apiCall.stopLoading();
          console.log(error);
        });
  }

  getVehicleList() {
    let that = this;
    // that.apiCall.startLoading().present();
    that.apiCall.getVehicleListCall(that.islogin._id, that.islogin.email)
      .subscribe(data => {
        // that.apiCall.stopLoading();
        that.portstemp = data.devices;
        // console.log("vehicle list=> " + that.portstemp)
      },
        err => {
          // that.apiCall.stopLoading();
          console.log(err);
        });
  }

  doInfinite(infiniteScroll) {
    let that = this;
    that.page = that.page + 1;
    setTimeout(() => {
      if (localStorage.getItem("filterByType") != null) {
        that.filterByType();
      } else {
        if (localStorage.getItem("filterByDate") != null) {
          that.filterByDate();
        } else {
          if (that.selectedVehicle != undefined) {
            that.temp(that.selectedVehicle);
          } else {
            that.apiCall.getDataOnScroll(that.islogin._id, that.page, that.limit)
              .subscribe(
                res => {
                  that.ndata = res;

                  for (let i = 0; i < that.ndata.length; i++) {
                    that.MassArray1.push(that.ndata[i]);
                  }
                },
                error => {
                  console.log(error);
                });
          }
        }
      }
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 1000);
  }
}
